package com.rufino.bloconotas

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.rufino.bloconotas.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val preferencia = PreferenciaAnotacao(applicationContext)
        val botaoSalvar = binding.fab

        botaoSalvar.setOnClickListener{
            val anotacaoRecuperado = binding.editContainer.editAnotacao.text.toString()

            if (anotacaoRecuperado.isEmpty()){
                Toast.makeText(this, "Digite alguma coisa", Toast.LENGTH_SHORT).show()
            }else{
                preferencia.SalvarAnotacao(anotacaoRecuperado)
                Toast.makeText(this, "Anotação salva com sucesso", Toast.LENGTH_SHORT).show()
            }
        }

        val anotacao = preferencia.RecuperarAnotacao().toString()
        if(anotacao.isNotEmpty()){
            binding.editContainer.editAnotacao.setText(anotacao)
        }
    }
}